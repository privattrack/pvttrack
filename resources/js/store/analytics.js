export default {
    namespaced: true,
    state: {
        campaign: '',
        visitors: [],
        opens: 0,
        clicks: 0,
        browsers: [],
        devices: [],
        // openers: [],
        // clickers: [],
        countries: [],
        isps: [],
        os: [],
        files: [],
        events: {
            device: 'opens',
            browser: 'opens',
        }
    },
    getters: {
        campaign: state => state.campaign,
        events: state => state.events,
        clicks: state => state.clicks,
        opens: state =>  state.opens,
        visitorsCount: state => state.visitors,
        actionsData(state){
            if(state.opens > 0){
                let analyticsData = [
                    { 'actionType': 'Opens', 'counts': state.opens, color: 'primary' },
                    { 'actionType': 'Clicks', 'counts': state.clicks, color: 'warning' },
                ]
                let total = state.visitors;
                let cp = parseInt((state.clicks / state.opens) * 100);
                let op = parseInt((state.opens / total) * 100);

                return {series: [op,cp], analyticsData: analyticsData};
            }
            return {series: [0,0], analyticsData: []};
        },
        devicesData(state){
            if(state.opens > 0){
                let selected = [];
                let total = 0;
                if(state.events.device == 'opens'){
                    selected = state.devices[0];
                    total = state.opens;
                }else if(state.events.device == 'clicks'){
                    selected = state.devices[1];
                    total = state.clicks;
                }
                let md = selected.filter(i => i.device == "smartphone")[0];
                let dd = selected.filter(i => i.device == "desktop")[0];
                let td = selected.filter(i => i.device == "tablet")[0];
                let od = selected.filter(i => (i.device == "unknown" || i.device == null))[0];

                let mp = (md.count / total) * 100;
                let dp = (dd.count / total) * 100;
                let tp = (td.count / total) * 100;
                let op = (od.count / total) * 100;
                
                if(isNaN(mp)) mp = 0 
                if(isNaN(dp)) dp = 0 
                if(isNaN(tp)) tp = 0 
                if(isNaN(op)) op = 0 

                let analyticsData = [
                    { device: 'Mobile', icon: 'SmartphoneIcon', color: 'warning', sessionsPercentage: parseFloat(mp).toFixed(1) },
                    { device: 'Dekstop', icon: 'MonitorIcon', color: 'primary', sessionsPercentage: parseFloat(dp).toFixed(1) },
                    { device: 'Tablet', icon: 'TabletIcon', color: 'danger', sessionsPercentage: parseFloat(tp).toFixed(1) },
                    { device: 'Others', icon: 'MoreHorizontalIcon', color: 'secondary', sessionsPercentage: parseFloat(op).toFixed(1) },
                ]
                let series = [dp,mp,tp,op];
                return {series: series, analyticsData: analyticsData};
            }
            return {series: [0,0,0], analyticsData: []}
        },
        browsersData(state){
            if(state.opens > 0){
                let key = "browser";
                let result = {};
                let selected = [];
                let total = 0;
                if(state.events.browser == 'opens'){
                    selected = state.browsers[0];
                    total = state.opens;
                }else if(state.events.browser == 'clicks'){
                    selected = state.browsers[1];
                    total = state.clicks;
                }
                selected.forEach(item => {
                    if (!result[item[key]]){
                        result[item[key]] = []
                    }
                    result[item[key]].push(item)
                });
                let browsers = [];
                for(let browser in result){
                    let ratio = parseInt((selected.count/total) * 100)
                    let data = {name: browser, ratio: ratio}
                    browsers.push(data);
                }
                browsers = browsers.sort((b1, b2) => b2.ratio - b1.ratio);
                return browsers;
            }
        },
        countriesData(state){
            return state.countries;
        },
        ispData(state){
            return state.isps;
        },
        osData(state){
            return state.os;
        },
        filesData(state){
            return state.files;
        }
    },
    mutations: {
        UPDATE_STATS(state, payload){
            state.countries = (payload.countries) ? payload.countries : [];
            state.isps = (payload.isp) ? payload.isp : [];
            state.os = (payload.os) ? payload.os : [];
            state.clickers = (payload.clickers) ? payload.clickers : [];
            state.openers = (payload.openers) ? payload.openers : [];
            state.visitors = (payload.visitors) ? payload.visitors : [];
            state.files = (payload.files) ? payload.files : [];
        },
        CHANGE_EVENT(state, data){
            if(data.from == 'device'){
                state.events.device = data.event;
            }else if(data.from == 'browser'){
                state.events.browser = data.event;
            }
        },
        UPDATE_CAMPAIGN(state, id){
            state.campaign = id
        },
        RESET_STATS(state){
            state.countries = [];
            state.isps = [];
            state.os = [];
            state.clickers = [];
            state.openers = [];
            state.visitors = [];
            state.files = [];
        }
    },
    actions: {
        fetchStats({commit}, id){
            return new Promise((resolve,reject) => {
                axios.get(`/api/stats/${id}`)
                .then((res) => {
                    commit('UPDATE_CAMPAIGN', id);
                    commit('UPDATE_STATS', res.data);
                    resolve(res);
                })
            })
        },
        reset({commit}){
            commit('RESET_STATS');
        }
    }
}