#!/bin/bash

echo -e "\033[1;32mDownload Completed..\033[0m"

cp .env.example .env

echo -e "\033[1;32mBuilding the docker environment..\033[0m"

docker-compose build &> /dev/null

echo -e "\033[1;32mStarting the services..\033[0m"

docker-compose up -d

echo -e "\033[1;32mBuilding the depencies..\033[0m"

docker-compose exec laravel-app composer install &> /dev/null

echo -e "\033[1;32mGenerating app key..\033[0m"

docker-compose exec laravel-app php artisan key:generate &> /dev/null

echo -e "\033[1;32mGenerating the database..\033[0m"

docker-compose exec laravel-app php artisan migrate &> /dev/null

echo -e "\033[1;32mGenerating auth secret..\033[0m"

docker-compose exec laravel-app php artisan jwt:secret &> /dev/null

clear

echo -e "\033[1;32mSetting up cron jobs..\033[0m"

echo "not done yet"

# 5 * * * * /usr/local/bin/docker-compose exec laravel-app php crons/crons.php

echo -e "\033[1;32mInstallation complete..Happy tracking!\033[0m"