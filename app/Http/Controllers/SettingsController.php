<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\User;
use Auth;
use App\Campaign;

class SettingsController extends Controller
{
    public function getSettings(){
        $token = config('app.agent_token');
        return ['token' => $token];
    }

	public function createUser(Request $request){
        $user = new User();
        $user->name = "admin";
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
		$user->save();
		return '1';
	}
	
	public function checkUser(){
        $user = User::all();
        return [$user];
    }
    public function updateToken(Request $request){
        $this->updateDotEnv('AGENT_TOKEN', $request->token);
	}
	
	public function updatePassword(Request $request)
	{
		$user = Auth::user();
		if (Hash::check($request->old, $user->password)) {
			$user->password = Hash::make($request->new);
			$user->save();
			return 1;
		}
		return 0;
	}

    public function updateDotEnv($key, $newValue, $delim=''){

	    $path = base_path('.env');
	    // get old value from current env
	    $oldValue = env($key);

	    // was there any change?
	    if ($oldValue === $newValue) {
	        return;
	    }

	    // rewrite file content with changed data
	    if (file_exists($path)) {
	        // replace current value with new value 
	        file_put_contents(
	            $path, str_replace(
	                $key.'='.$delim.$oldValue.$delim, 
	                $key.'='.$delim.$newValue.$delim, 
	                file_get_contents($path)
	            )
	        );
	    }
	}
	
	public function test()
	{
	    echo "<pre>";
        $campaign = Campaign::find(10);
        $stats = $campaign->stats;
        $countries = json_decode($stats->countries);
        $visitors = $campaign->emails;
        $isp = json_decode($stats->isps);
        $os = json_decode($stats->os);
        $devices = json_decode($stats->devices);
        $browsers = json_decode($stats->browsers);
        $opens = json_decode($campaign->stats->opens);
        $clicks = json_decode($campaign->stats->clicks);
        $files = json_decode($campaign->files);
        $load = ['countries' => $countries, 'isp' => $isp, 'os' => $os, 'opens' => $opens, 'clicks' => $clicks, 'browsers' => $browsers, 'devices' => $devices, 'visitors' => $visitors, 'files' => $files];
        print_r($load);
	}
}
