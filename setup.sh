#!/bin/bash

sudo apt install openssh-server

service ssh start

echo -e "\033[1;32mDownloading the tracker..\033[0m"

# wget https://www.dropbox.com/sh/c9dmov85dww4osf/AABWiWxyvXkFiWRYykjLwVVda?dl=1

echo -e "\033[1;32mChecking DOCKER!\033[0m"

if ! $(docker ps &> /dev/null) ; then

	echo -e "\033[1;32mDocker is not running..Starting docker\033[0m"

	if ! $(sudo systemctl start docker &> /dev/null) ; then

		echo -e "\033[1;32mDocker not installed..Installing\033[0m"

		sudo yum install -y yum-utils device-mapper-persistent-data lvm2 &> /dev/null

		sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo &> /dev/null

		sudo yum install -y docker &> /dev/null

		echo -e "\033[1;32mDocker installed..Starting docker\033[0m"

		if ! $(sudo systemctl start docker &> /dev/null) ; then
			echo "Docker is not properly installed..exiting"
			exit 1
		fi

		sudo systemctl enable docker &> /dev/null

		if ! $(docker ps &> /dev/null) ; then
			echo "Docker is not properly installed..exiting"
			exit 1
		fi
	fi
fi

echo -e "\033[1;32mDocker is running..Checking docker-compose\033[0m"

if ! $(docker-compose --version &> /dev/null) ; then
	echo -e "\033[1;32mDocker compose is not installed..Installing\033[0m"

	sudo curl -L "https://github.com/docker/compose/releases/download/1.23.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose &> /dev/null

	sudo chmod +x /usr/local/bin/docker-compose &> /dev/null
	
	if ! $(docker-compose --version &> /dev/null) ; then
		echo "Docker compose is not properly installed..exiting"
		exit 1
	fi
fi
clear
echo -e "\033[1;32mDocker and docker-compose are running..\033[0m"

if [ "$1" == "install" ]
then
	./install.sh
elif [ "$1" == "restart" ]
	./restart.sh
else
	./install.sh
fi


